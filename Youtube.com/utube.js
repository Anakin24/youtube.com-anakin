var utubeVid = document.getElementById('utubeVid');
var vidOut = document.getElementById('vidOut');
var vidPlay = document.getElementById('vidPlay');
var vidPause = document.getElementById('vidPause');
var videoControlers = document.getElementById('videoControlers');
var vidIcon = document.getElementById('vidIcon');
var vidDuration = document.getElementById('vidDuration');
var vidCurrentDuration = document.getElementById('vidCurrentDuration');
var vidVolumeUp = document.getElementById('vidVolumeUp');
var vidVolumeDown = document.getElementById('vidVolumeDown');
var fullScreen = document.getElementById('fullScreen');

vidPlay.addEventListener('click' , function() {
    utubeVid.play();
    vidOut.style.display = 'none';
    vidIcon.innerZHTML = '<i class="fas fa-pause" id="vidPause"></i>';
});

vidIcon.addEventListener('click' , function() {
    if(utubeVid.paused) {
        utubeVid.play();
        vidIcon.innerHTML ='<i class="fas fa-pause" id="vidPause"></i>';
    } else {
        utubeVid.pause();
        vidIcon.innerHTML ='<i class="fas fa-play" id="vidPlayer"></i>';
    }
});
utubeVid.addEventListener('canplay' , function() {
    vidDuration.innerText = utubeVid.duration;
});
utubeVid.addEventListener('timeupdate' , function() {
    vidCurrentDuration.innerText = utubeVid.currentTime;
});
vidVolumeDown.addEventListener('click' , function(){
    if(utubeVid.volume - 0.1 < 0) {
        utubeVid.volume = 0;
        vidVolumeDown.innerHTML = '<i class="fas fa-volume-mute" id="vidVolumeMute"></i>';
        return;
    }
    utubeVid.volume = utubeVid.volume - 0.5;
});

vidVolumeUp.addEventListener('click' , function() {
    if(utubeVid.volume + 0.1 > 1) {
        utubeVid.volume = 1;
        return;
    }
    utubeVid.volume = utubeVid.volume + 0.5 ;
    if(utubeVid.volume = utubeVid.volume + 0.5) {
        vidVolumeDown.innerHTML = '<i class="fas fa-volume-down"></i>'
    }
});

utubeVid.onended = function() {
    vidIcon.innerHTML = '<i class="fas fa-play" id="vidPlayer"></i>';
    vidOut.style.display = 'block';
};


fullScreen.addEventListener('click' , function() { {
        if(utubeVid.requestFullscreen) {
            utubeVid.requestFullscreen();
        }else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen();
    }
    }
    
})